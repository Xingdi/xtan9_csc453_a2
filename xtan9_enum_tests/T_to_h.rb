require "../xtan9_enum/enum.rb"

class Array
	include P2Enumerable
	alias p2each each
end

class Hash
	include P2Enumerable
	alias p2each each
end

def to_h_test1
	b = { 'a'=>'b', 'c'=>'d', 'e'=>'f' }.p2to_h
	raise "#{__method__} error" if b != {"a"=>"b", "c"=>"d", "e"=>"f"}
  	p "#{__method__} passed"
end



def to_h_test
	to_h_test1
	
end

module To_h_test
	to_h_test
end