require "../xtan9_enum/enum.rb"

class Array
	include P2Enumerable
	alias p2each each
end

class Hash
	include P2Enumerable
	alias p2each each
end




def all_test1
	b = %w[ant bear cat].p2all? { |word| word.length >= 3 } 
	raise "#{__method__} error" if !b == true
  	p "#{__method__} passed"
end

def all_test2
	b = %w[ant bear cat].p2all? { |word| word.length >= 4 } 
	raise "#{__method__} error" if !b == false
  	p "#{__method__} passed"
end

def all_test3
	b = %w[anta bear cata].p2all? { |word| word.length == 4 } 
	raise "#{__method__} error" if !b == true
  	p "#{__method__} passed"
end

def all_test4
	h = Hash[[["hello",5],["hell",4],["hel",3],["he",2]]]
	b = h.p2all? {|key,value| key.length == value}
	raise "#{__method__} error" if !b == true
  	p "#{__method__} passed"
end

 def all_test5
	h = Hash[[["hello",5],["hell",3],["hel",3],["he",2]]]
	b = h.p2all? {|key,value| key.length == value}
	raise "#{__method__} error" if !b == false
  	p "#{__method__} passed"
end

def all_test6
	h = Hash[[["hello",7],["hell",4],["hel",3],["he",2]]]
	b = h.p2all? {|key,value| key.length == value}
	raise "#{__method__} error" if !b == false
  	p "#{__method__} passed"
end

def all_test
	all_test1
	all_test2
	all_test3
	all_test4
	all_test5
	all_test6
end

module All_test
	all_test
end

