require "../xtan9_enum/enum.rb"

class Array
	include P2Enumerable
	alias p2each each
end

class Hash
	include P2Enumerable
	alias p2each each
end

def inject_test1
	b = [5,6,7,8,9,10].p2para0inject { |sum, n| sum + n }           #=> 45
	raise "#{__method__} error" if b != 45
  	p "#{__method__} passed"
end

def inject_test2
	b = [5,6,7,8,9,10].p2inject(1) { |product, n| product * n } #=> 151200
	raise "#{__method__} error" if b != 151200
  	p "#{__method__} passed"
end

module Group_by_test
	inject_test1
	inject_test2
end