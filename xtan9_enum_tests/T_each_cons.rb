require "../xtan9_enum/enum.rb"

class Array
	include P2Enumerable
	alias p2each each
end

class Hash
	include P2Enumerable
	alias p2each each
end

def each_cons_test1
	ar = []
	b = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10].p2each_cons(3){|a| ar << a} #######################
	
	raise "#{__method__} error" if b != nil or ar != [[1, 2, 3],[2, 3, 4],[3, 4, 5],[4, 5, 6],[5, 6, 7],[6, 7, 8],[7, 8, 9],[8, 9, 10]]
  	p "#{__method__} passed"
end

def each_cons_test2
	h = Hash[[[1,1],[2,3],[3,2],[5,2]]]
	ar = []
	b = h.p2each_cons(2){|a| ar << a}#####################
	raise "#{__method__} error" if b != nil or ar != [[[1, 1], [2, 3]],[[2, 3], [3, 2]],[[3, 2], [5, 2]]]
  	p "#{__method__} passed"
end


def each_cons_test
	each_cons_test1
	each_cons_test2
end

module Each_cons_test
	each_cons_test
end