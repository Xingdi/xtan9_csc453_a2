require "../xtan9_enum/enum.rb"

class Array
	include P2Enumerable
	alias p2each each
end

class Hash
	include P2Enumerable
	alias p2each each
end

def detect_test1
	b = [1, 2, 3, 4].p2detect(2) { |x| x%2 == 0 and x%4 ==0}
	raise "#{__method__} error" if b != 4
  	p "#{__method__} passed"
end

def detect_test2
	b = [1, 2, 3].p2detect(2) { |x| x%2 == 0 and x%4 ==0}
	raise "#{__method__} error" if b != 2
  	p "#{__method__} passed"
end

def detect_test3
	b = [1, 2, 3].p2detect() { |x| x%2 == 0 and x%4 ==0}
	raise "#{__method__} error" if b != nil
  	p "#{__method__} passed"
end

def detect_test4
	h = Hash[[[1,1],[2,3],[3,2],[4,1]]]
	b = h.detect{|e| e == [3,2]}
	raise "#{__method__} error" if b != [3, 2]
  	p "#{__method__} passed"
end



def detect_test
	detect_test1
	detect_test2
	detect_test3
	detect_test4
end

module Detect_test
	detect_test
end








