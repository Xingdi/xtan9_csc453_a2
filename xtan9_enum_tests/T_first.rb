require "../xtan9_enum/enum.rb"

class Array
	include P2Enumerable
	alias p2each each
end

class Hash
	include P2Enumerable
	alias p2each each
end

def first_test1
	b = %w[foo bar baz].p2first(2)  #=> ["foo", "bar"]#######################
	raise "#{__method__} error" if b != ["foo", "bar"]
  	p "#{__method__} passed"
end

def first_test2
	b = { 'a'=>1, 'b'=>2, 'c'=>3 }.p2first(1) #######################
	raise "#{__method__} error" if b != [["a",1]]
  	p "#{__method__} passed"
end

def first_test
	first_test1
	first_test2
end

module First_test
	first_test
end