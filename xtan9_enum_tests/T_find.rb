require "../xtan9_enum/enum.rb"

class Array
	include P2Enumerable
	alias p2each each
end

class Hash
	include P2Enumerable
	alias p2each each
end

def find_test1
	b = [1, 2, 3, 4].p2find(2) { |x| x%2 == 0 and x%4 ==0}
	raise "#{__method__} error" if b != 4
  	p "#{__method__} passed"
end

def find_test2
	b = [1, 2, 3].p2find() { |x| x%2 == 0 and x%4 ==0}
	raise "#{__method__} error" if b != nil
  	p "#{__method__} passed"
end

def find_test3
	b = [1, 2, 3].p2find() { |x| x%2 == 0 and x%4 ==0}
	raise "#{__method__} error" if b != nil
  	p "#{__method__} passed"
end

def find_test4
	h = Hash[[[1,1],[2,3],[3,2],[4,1]]]
	b = h.p2find{|e| e == [3,2]}
	raise "#{__method__} error" if b != [3, 2]
  	p "#{__method__} passed"
end

def find_test
	find_test1
	find_test2
	find_test3
	find_test4
end

module Find_test
	find_test
end