require "../xtan9_enum/enum.rb"

class Array
	include P2Enumerable
	alias p2each each
end

class Hash
	include P2Enumerable
	alias p2each each
end

def drop_while_test1
	b = [1, 2, 3, 4, 5, 0].p2drop_while{|i| i < 3}
	raise "#{__method__} error" if b != [3, 4, 5, 0]
  	p "#{__method__} passed"
end

def drop_while_test2
	b = [1, 2, 3, 4, 1, 5, 0].p2drop_while{|i| i < 4}
	raise "#{__method__} error" if b != [4, 1, 5, 0]
  	p "#{__method__} passed"
end

def drop_while_test3
	h = Hash[[[1,1],[2,3],[3,2],[1,1]]]
	b = h.p2drop_while{|x,y| x == y}
	raise "#{__method__} error" if b != [[1,1],[2, 3], [3, 2]]
  	p "#{__method__} passed"
end


def drop_while_test
	drop_while_test1
	drop_while_test2
	drop_while_test3
end

module Drop_while_test
	drop_while_test
end