require "../xtan9_enum/enum.rb"

class Array
	include P2Enumerable
	alias p2each each
end

class Hash
	include P2Enumerable
	alias p2each each
end



def collect_concat_test2
	b = [[1, 2], [3, 4]].p2collect_concat { |e| e + [100] }
	raise "#{__method__} error" if b != [1, 2, 100, 3, 4, 100]
  	p "#{__method__} passed"
end

def collect_concat_test3
	b = [1, 2, 3, 4].p2collect_concat { |e| e  }
	raise "#{__method__} error" if b != [1, 2, 3, 4]
  	p "#{__method__} passed"
end

def collect_concat_test4
	h = Hash[[[1,1],[2,3],[3,2],[4,1]]]
	b = h.p2collect_concat{|e| e}
	raise "#{__method__} error" if b != [1, 1, 2, 3, 3, 2, 4, 1]
  	p "#{__method__} passed"
end

def collect_concat_test5
	h = Hash[[[1,1],[2,3],[3,2],[4,1]]]
	b = h.p2collect_concat{|e| e +[100]}
	raise "#{__method__} error" if b != [1, 1, 100, 2, 3, 100, 3, 2, 100, 4, 1, 100]
  	p "#{__method__} passed"
end


def collect_concat_test
	
	collect_concat_test2
	collect_concat_test3
	collect_concat_test4
	collect_concat_test5
end

module Collect_concat_test
	collect_concat_test
end