require "../xtan9_enum/enum.rb"

class Array
	include P2Enumerable
	alias p2each each
end

class Hash
	include P2Enumerable
	alias p2each each
end

def find_index_test1
	b = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10].p2find_index{ |i| i % 2 == 0 and i % 4 == 0 }  #=> nil #######################
	raise "#{__method__} error" if b != 3
  	p "#{__method__} passed"
end

def find_index_test2
	b = { 'a'=>1, 'b'=>2, 'c'=>3 }.p2find_index{|x,y|  y % 4 == 0 } #######################
	raise "#{__method__} error" if b != nil
  	p "#{__method__} passed"
end

def find_index_test
	find_index_test1
	find_index_test2
end

module Find_index_test
	find_index_test
end



