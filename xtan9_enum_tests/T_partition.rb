require "../xtan9_enum/enum.rb"

class Array
	include P2Enumerable
	alias p2each each
end

class Hash
	include P2Enumerable
	alias p2each each
end

def partition_test1
	b =[1,2,3,4,5,6].p2partition { |v| v.even? }  #=> [[2, 4, 6], [1, 3, 5]] #######################
	raise "#{__method__} error" if b != [[2, 4, 6], [1, 3, 5]]
  	p "#{__method__} passed"
end

def partition_test2
	b = { 'a'=>1, 'b'=>2, 'c'=>3 }.p2partition{|x,y|  y.even? } #######################
	raise "#{__method__} error" if b != [[["b", 2]], [["a", 1], ["c", 3]]]
  	p "#{__method__} passed"
end

def partition_test
	partition_test1
	partition_test2
end

module Partition_test
	partition_test
end