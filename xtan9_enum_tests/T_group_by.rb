require "../xtan9_enum/enum.rb"

class Array
	include P2Enumerable
	alias p2each each
end

class Hash
	include P2Enumerable
	alias p2each each
end

def group_by_test1
	b = [1,2,3,4,5,6].p2group_by { |i| i%3 }   #=> {0=>[3, 6], 1=>[1, 4], 2=>[2, 5]} #######################
	raise "#{__method__} error" if b != {0=>[3, 6], 1=>[1, 4], 2=>[2, 5]}
  	p "#{__method__} passed"
end

def group_by_test2
	b = { 'a'=>1, 'b'=>2, 'c'=>3 }.p2group_by{|x,y| y%2} #######################
	raise "#{__method__} error" if b != {1=>[["a", 1], ["c", 3]], 0=>[["b", 2]]}
  	p "#{__method__} passed"
end

def group_by_test
	group_by_test1
	group_by_test2
end

module Group_by_test
	group_by_test
end