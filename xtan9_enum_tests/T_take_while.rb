require "../xtan9_enum/enum.rb"

class Array
	include P2Enumerable
	alias p2each each
end

class Hash
	include P2Enumerable
	alias p2each each
end

def take_while_test1
	a = [1, 2, 3, 4, 5, 0]
	b = a.p2take_while { |i| i < 3 }   #=> [1, 2]
	raise "#{__method__} error" if b != [1, 2] 
  	p "#{__method__} passed"
end

def take_while_test2
	b = { 'a'=>1, 'b'=>2, 'c'=>1 }.p2take_while{|x,y| y<2}
	raise "#{__method__} error" if b != [["a", 1]] 
	p "#{__method__} passed"
end

def take_while_test
	take_while_test1
	take_while_test2
end

module Take_while_test
	take_while_test
end