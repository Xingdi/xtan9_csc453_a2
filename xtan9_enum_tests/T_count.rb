require "../xtan9_enum/enum.rb"

class Array
	include P2Enumerable
	alias p2each each
end

class Hash
	include P2Enumerable
	alias p2each each
end

def count_test1
	b = [1, 2, 4, 2].p2count { |e| e%2==0 }
	raise "#{__method__} error" if b != 3
  	p "#{__method__} passed"
end

def count_test2
	b = [1, 2, 4, 2].p2count { |e| e%1==0 }
	raise "#{__method__} error" if b != 4
  	p "#{__method__} passed"
end

def count_test2
	b = [1, 2, 4, 2].p2count { |e| e%1==0 }
	raise "#{__method__} error" if b != 4
  	p "#{__method__} passed"
end

def count_test3
	h = Hash[[[1,1],[2,3],[3,2],[4,1]]]
	b = h.p2count{|x,y| x>y}
	raise "#{__method__} error" if b != 2
  	p "#{__method__} passed"
end

def count_test
	count_test1
	count_test2
	count_test3
end

module Count_test
	count_test
end