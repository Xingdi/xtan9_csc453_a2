require "../xtan9_enum/enum.rb"

class Array
	include P2Enumerable
	alias p2each each
end

class Hash
	include P2Enumerable
	alias p2each each
end

def to_a_test1
	b = [1, 2, 3, 4, 5, 6, 7].p2to_a() #######################
	raise "#{__method__} error" if b != [1, 2, 3, 4, 5, 6, 7]
  	p "#{__method__} passed"
end

def to_a_test2
	b = { 'a'=>1, 'b'=>2, 'c'=>3 }.p2to_a() #######################
	raise "#{__method__} error" if b != [["a", 1], ["b", 2], ["c", 3]]
  	p "#{__method__} passed"
end

def to_a_test
	to_a_test1
	to_a_test2
end

module To_a_test
	to_a_test
end