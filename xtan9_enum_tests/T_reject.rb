require "../xtan9_enum/enum.rb"

class Array
	include P2Enumerable
	alias p2each each
end

class Hash
	include P2Enumerable
	alias p2each each
end

def reject_test1
	b = [1,2,3,4,5,6,7,8,9,10].p2reject { |i|  i % 3 == 0 }   #=> [1, 2, 4, 5, 7, 8, 10] #######################
	raise "#{__method__} error" if b != [1, 2, 4, 5, 7, 8, 10]
  	p "#{__method__} passed"
end

def reject_test2
	b = Hash[ 'a'=>1, 'b'=>2, 'c'=>3].p2reject{|x,y|  y % 3 == 0 } #######################
	raise "#{__method__} error" if b != [["a", 1], ["b", 2]]
  	p "#{__method__} passed"
end

def reject_test
	reject_test1
	reject_test2
end

module Reject_test1
	reject_test
end