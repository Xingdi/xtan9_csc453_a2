require "../xtan9_enum/enum.rb"

class Array
	include P2Enumerable
	alias p2each each
end

class Hash
	include P2Enumerable
	alias p2each each
end

def collect_test1
	b = %w[ant bear cat].p2collect { "cat" } 
	raise "#{__method__} error" if b != ["cat","cat","cat"]
  	p "#{__method__} passed"
end

def collect_test2
	b = [1, 2, 3, 4].p2collect { "cat" } 
	raise "#{__method__} error" if b != ["cat","cat","cat","cat"]
  	p "#{__method__} passed"
end

def collect_test3
	b = [1, 2, 3, 4].p2collect { |y| y+1 } 
	raise "#{__method__} error" if b != [2,3,4,5]
  	p "#{__method__} passed"
end

def collect_test4
	h = Hash[[["hello",0],["hell",3],["hel",2],["he",1]]]
	b = h.p2collect{|k,y| ["ji",4]}
	raise "#{__method__} error" if b != [["ji", 4], ["ji", 4], ["ji", 4], ["ji", 4]]
  	p "#{__method__} passed"
end

def collect_test5
	h = Hash[[["hello",0],["hell",3],["hel",2],["he",1]]]
	b = h.p2collect{|k| 5}
	raise "#{__method__} error" if b != [5, 5, 5, 5]
  	p "#{__method__} passed"
end

def collect_test6
	h = Hash[[[1,1],[2,3],[3,2],[4,1]]]
	b = h.p2collect{|x,y| [x*2,y*2]}
	raise "#{__method__} error" if b != [[2, 2], [4, 6], [6, 4], [8, 2]]
  	p "#{__method__} passed"
end


def collect_test
	collect_test1
	collect_test2
	collect_test3
	collect_test4
	collect_test5
	collect_test6
end

module Collect_test
	collect_test
end
