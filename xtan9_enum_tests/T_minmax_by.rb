require "../xtan9_enum/enum.rb"

class Array
	include P2Enumerable
	alias p2each each
end

class Hash
	include P2Enumerable
	alias p2each each
end

def minmax_by_test1
	a = ["albatross", "dog", "horse"]
	b = a.p2minmax_by { |a| a.length } #=> ["dog", "albatross"]                                #=> ["albatross", "horse"]
	raise "#{__method__} error" if b != ["dog", "albatross"] 
  	p "#{__method__} passed"
end

def minmax_by_test2
	b = { 'a'=>1, 'b'=>2, 'c'=>3 }.p2minmax_by { |a| a.length}
	raise "#{__method__} error" if b != [["a", 1], ["a", 1]]
  	p "#{__method__} passed"
end

def minmax_by_test
	minmax_by_test1
	minmax_by_test2
end

module Minmax_by_test
	minmax_by_test
end