require "../xtan9_enum/enum.rb"

class Array
	include P2Enumerable
	alias p2each each
end

class Hash
	include P2Enumerable
	alias p2each each
end

def find_all_test1
	b = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10].p2find_all{|i|  i % 3 == 0 } #######################
	raise "#{__method__} error" if b != [3,6,9]
  	p "#{__method__} passed"
end

def find_all_test2
	b = { 'a'=>1, 'b'=>2, 'c'=>3 }.p2find_all{|x,y|  y % 3 == 0 } #######################
	raise "#{__method__} error" if b != [["c",3]]
  	p "#{__method__} passed"
end

def find_all_test
	find_all_test1
	find_all_test2
end

module Find_all_test1
	find_all_test
end