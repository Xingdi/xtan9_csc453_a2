require "../xtan9_enum/enum.rb"

class Array
	include P2Enumerable
	alias p2each each
end

class Hash
	include P2Enumerable
	alias p2each each
end

def entries_test1
	b = [1, 2, 3, 4, 5, 6, 7].p2entries() #######################
	raise "#{__method__} error" if b != [1, 2, 3, 4, 5, 6, 7]
  	p "#{__method__} passed"
end

def entries_test2
	b = { 'a'=>1, 'b'=>2, 'c'=>3 }.p2entries() #######################
	raise "#{__method__} error" if b != [["a", 1], ["b", 2], ["c", 3]]
  	p "#{__method__} passed"
end

def entries_test
	entries_test1
	entries_test2
end

module Entries_test
	entries_test
end