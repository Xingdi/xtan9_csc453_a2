require "../xtan9_enum/enum.rb"

class Array
	include P2Enumerable
	alias p2each each
end

class Hash
	include P2Enumerable
	alias p2each each
end

def take_test1
	a = [1, 2, 3, 4, 5, 0]
	b = a.p2take(3)             #=> [1, 2, 3]
	c = a.p2take(30)            #=> [1, 2, 3, 4, 5, 0] #######################
	raise "#{__method__} error" if b != [1, 2, 3] or c != [1, 2, 3, 4, 5, 0]
  	p "#{__method__} passed"
end

def take_test2
	b = { 'a'=>1, 'b'=>2, 'c'=>3 }.p2take(2) 
	c = { 'a'=>1, 'b'=>2, 'c'=>3 }.p2take(7)
	raise "#{__method__} error" if b != [["a", 1], ["b", 2]] or c != [["a", 1], ["b", 2], ["c", 3]]
  	p "#{__method__} passed"
end

def take_test
	take_test1
	take_test2
end

module Take_test
	take_test
end