require "../xtan9_enum/enum.rb"

class Array
	include P2Enumerable
	alias p2each each
end

class Hash
	include P2Enumerable
	alias p2each each
end

def drop_test1
	b = [1, 2, 3, 4, 5, 0].p2drop(3)
	raise "#{__method__} error" if b != [4, 5, 0]
  	p "#{__method__} passed"
end

def drop_test2
	h = Hash[[[1,1],[2,3],[3,2],[4,1]]]
	b = h.p2drop(2)
	raise "#{__method__} error" if b != [[3,2], [4,1]]
  	p "#{__method__} passed"
end


def drop_test
	drop_test1
	drop_test2
end

module Drop_test
	drop_test
end