require "../xtan9_enum/enum.rb"

class Array
	include P2Enumerable
	alias p2each each
end

class Hash
	include P2Enumerable
	alias p2each each
end

def each_with_index_test1
	hash = Hash.new
	%w(cat dog wombat).p2each_with_index { |item, index| hash[item] = index}
     
	raise "#{__method__} error" if hash !={"cat"=>0, "dog"=>1, "wombat"=>2}
  	p "#{__method__} passed"
end

def each_with_index_test
	each_with_index_test1
end

module Each_with_index_test1
	each_with_index_test
end
