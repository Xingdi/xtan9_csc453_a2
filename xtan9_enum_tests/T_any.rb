require "../xtan9_enum/enum.rb"

class Array
	include P2Enumerable
	alias p2each each
end

class Hash
	include P2Enumerable
	alias p2each each
end

def any_test1
	b = %w[ant bear cat].p2any? { |word| word.length >= 3 } #=> true
	raise "#{__method__} error" if !b == true
  	p "#{__method__} passed"
end

def any_test2
	b = %w[ant bear cat].p2any? { |word| word.length >= 4 } #=> true
	raise "#{__method__} error" if !b == true
  	p "#{__method__} passed"
end

def any_test3
	b = %w[anta bear cata].p2any? { |word| word.length == 4 } 
	raise "#{__method__} error" if !b == true
  	p "#{__method__} passed"
end

def any_test4
	b = %w[anta bea cat].p2any? { |word| word.length == 4 } 
	raise "#{__method__} error" if !b == true
  	p "#{__method__} passed"
end

def any_test5
	b = %w[ant bea cat].p2any? { |word| word.length == 4 } 
	raise "#{__method__} error" if !b == false
  	p "#{__method__} passed"
end

def any_test6
	b = %w[antee bea cat].p2any? { |word| word.length == 4 } 
	raise "#{__method__} error" if !b == false
  	p "#{__method__} passed"
end





def any_test7
	h = Hash[[["hello",5],["hell",4],["hel",3],["he",2]]]
	b = h.p2any? {|key,value| key.length == value}
	raise "#{__method__} error" if !b == true
  	p "#{__method__} passed"
end

def any_test8
	h = Hash[[["hello",5],["hell",3],["hel",3],["he",2]]]
	b = h.p2any? {|key,value| key.length == value}
	raise "#{__method__} error" if !b == true
  	p "#{__method__} passed"
end

def any_test9
	h = Hash[[["hello",7],["hell",4],["hel",3],["he",2]]]
	b = h.p2any? {|key,value| key.length == value}
	raise "#{__method__} error" if !b == true
  	p "#{__method__} passed"
end

def any_test10
	h = Hash[[["hello",7],["hell",5],["hel",1],["he",4]]]
	b = h.p2any? {|key,value| key.length == value}
	raise "#{__method__} error" if !b == false
  	p "#{__method__} passed"
end


def any_test11
	h = Hash[[["hello",1],["hell",2],["hel",2],["he",2]]]
	b = h.p2any? {|key,value| key.length == value}
	raise "#{__method__} error" if !b == true
  	p "#{__method__} passed"
end


def any_test12
	h = Hash[[["hello",0],["hell",3],["hel",2],["he",1]]]
	b = h.p2any? {|key,value| key.length == value}
	raise "#{__method__} error" if !b == false
  	p "#{__method__} passed"
end


def any_test
	any_test1
	any_test2
	any_test3
	any_test4
	any_test5
	any_test6
	any_test7
	any_test8
	any_test9
	any_test10
	any_test11
	any_test12
end

module Any_test
	any_test
end