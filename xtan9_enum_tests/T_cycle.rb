require "../xtan9_enum/enum.rb"

class Array
	include P2Enumerable
	alias p2each each
end

class Hash
	include P2Enumerable
	alias p2each each
end

def cycle_test1
	arr = []
	b = ["a", "b", "c"].p2cycle(2) { |x| arr << x }
	raise "#{__method__} error" if b != nil or arr != ["a", "b", "c", "a", "b", "c"]
  	p "#{__method__} passed"
end

def cycle_test2
	arr = []
	b = ["a", "b", "c"].p2cycle(-2){ |x| arr << x }
	raise "#{__method__} error" if b != nil
  	p "#{__method__} passed"
end

def cycle_test3
	arr = []
	b = [1, 2, 3].p2cycle(5){ |x| arr << x+1 }
	raise "#{__method__} error" if b != nil or arr != [2, 3, 4, 2, 3, 4, 2, 3, 4, 2, 3, 4, 2, 3, 4]
  	p "#{__method__} passed"
end

def cycle_test4
	h = Hash[[[1,1],[2,3],[3,2],[4,1]]]
	b = h.p2cycle(-2){ |x,y| [x+1,y+1] }
	raise "#{__method__} error" if b != nil
  	p "#{__method__} passed"
end


def cycle_test
	cycle_test1
	cycle_test2
	cycle_test3
	cycle_test4
end

module Cycle_test
	cycle_test
end