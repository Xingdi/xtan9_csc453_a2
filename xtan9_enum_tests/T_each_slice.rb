require "../xtan9_enum/enum.rb"

class Array
	include P2Enumerable
	alias p2each each
end

class Hash
	include P2Enumerable
	alias p2each each
end

def each_slice_test1
	ar = []
	b = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10].p2each_slice(3){|a| ar << a} #######################
	raise "#{__method__} error" if b != nil or ar != [[1, 2, 3],[4, 5, 6],[7, 8, 9],[10]]
  	p "#{__method__} passed"
end

def each_slice_test2
	h = Hash[[[1,1],[2,3],[3,2],[5,2]]]
	ar = []
	b = h.p2each_slice(2){|a| ar << a}#####################
	raise "#{__method__} error" if b != nil or ar != [[[1, 1], [2, 3]],[[3, 2], [5, 2]]]
  	p "#{__method__} passed"
end

def each_slice_test
	each_slice_test1
	each_slice_test2
end

module Each_slice_test
	each_slice_test
end
