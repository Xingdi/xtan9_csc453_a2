

module P2Enumerable
	def p2all?(&blk)
		p2each do |e|
			if blk.call(e) == false
				return false
			end
		end
		return true
	end

	def p2any?(&blk)
		p2each do |e|
			if blk.call(e) == true
				return true
			end
		end
		return false
	end

	def p2collect(&blk)
		r = Array.new
		p2each do |e|
			n = blk.call(e)
			r << n
		end
		return r
	end

	def p2collect_concat
        res = Array.new
        self.p2each do |i|
            r = yield i
            if r.class != Array
                res << r
            else
                res += r
            end
        end
        return res
    end

	def p2count(&blk)
		r = 0
		p2each do |e|
			if blk.call(e) == true
			r += 1
			end
		end
		return r
	end

	def p2cycle(init = nil, &blk)
		if init == nil
			p2each do |e|
				blk.call(e)
			end
			p2cycle(nil,&blk)
		end
		return nil if init == 0
		p2each do |e|
            blk.call(e)
        end
		if init >0
			p2cycle(init-1, &blk)
		end
	end

	def p2detect(init = nil, &blk)
		c = nil
		p2each do |e|
			if blk.call(e) == true
				c =e
                return c
			end
		end
		if c != nil
			return c
		else
			return init
		end
	end

	def p2drop(init)
		n = init
		ar = Array.new
		c = 0
		p2each do |e|
			c += 1
			if c > n
				ar << e
			end
		end
		return ar
	end

	def p2drop_while()
    	r = []
    	s = 0
    	self.p2each do |e|
      		s +=1
     		if yield(s) == false
        		r.push(e)
      		end
      	end
      	return r
    end


	def p2each_cons(n)
        len = 0
        a = []
        p2each do |x|
            a << x
        end
        len = a.length

        i = 0
        self.p2each do |e|
            arr = Array.new
            j = 0
            self.p2each do |e|
                if i+n <= len and j < n + i and j >= i
                    arr << e
                end
                j += 1
            end
            if i+n <= len
                yield arr 
                i += 1
            else
                return 
            end
        end
    end

	def p2each_slice(init,&blk)
	    t = self
	    b = true
	    s = 0
	    size = self.size
	    c = size/init
	    if size% init != 0
	    	c +=1 
	    end
	    t.each do |e|
	     	if s == c
	     		return nil 
	     	end
	      	t = t.p2drop(init) if b == false
	      	t =t.p2drop(0) if b == true
	      	b = false
	      	s +=1
	      	blk.call(t[0,init])
	    end
	end

	def p2each_with_index(*args) 
        i = 0
        self.p2each do |e|
            yield e, i, args
            i += 1
        end
    end


	def p2entries(*args) 
        arr = Array.new
        self.p2each do |e|
            arr << e
        end
        return arr

	end

	def p2find(n= nil, &blk)
		p2detect(n= nil, &blk)
	end

	 def p2find_all()
    	arr = []
    	self.p2each do |e|
      		arr.push(e) if yield(e)
    	end
    	return arr
  	end


	def p2find_index
		s = 0
        self.p2each do |e|
            if yield e
                return s
            end
            s += 1
        end
        return nil
	end

	def p2first(init)
    	arr =[]
    	s = 0
    	p2each do |e|
      		arr << e if s < init
      		s += 1
    	end
    return arr
	end

	def p2group_by(&blk)
    	h = Hash.new
    	p2each do |e|
      		arr = []
      		p2each do |x|
        		arr << x if blk.call(x) === blk.call(e)
      		end
      		h[blk.call(e)] = arr
    	end
    	return h
  	end


	def p2inject(init, &blk)#referred to Professor Ding's lecture code
    	r = init
    	self.each do | e |
      		r = blk.call(r,e)
    	end
    	return r
  	end

	def p2para0inject #referred to Professor Ding's lecture code
		first = true
    	r = nil
    	self.each do | e |
      		if first
     			r = e
        		first = false
      		else
        		r = yield( r, e )
      		end
    	end
    	return r
	end

	def p2minmax(&blk) 
        min = nil
        max = nil
        first = true
        p2each do |i|
        	if first == true
        		min = i
        		max = i
        		first = false
        	else
        		if blk.call(i,min)<0
        			min = i
        		end
        		if blk.call(i,max)>0
        			max = i
        		end
        	end
        end
        return [min,max]
    end
	
	def p2minmax_by(&blk) 
        min = nil
        max = nil
        first = true
        p2each do |i|
        	if first == true
        		min = i
        		max = i
        		first = false
        	else
        		if blk.call(i)<blk.call(min)
        			min = i
        		end
        		if blk.call(i)>blk.call(max)
        			max = i
        		end
        	end
        end
        return [min,max]

	end

	def p2partition(&blk)
        t = []
        f = []
        p2each do |e|
        	if blk.call(e)
        		t << e
        	else
        		f << e
        	end
        end
        return [t, f]
    end

	def p2reject
		res = Array.new
        self.p2each do |i|
            if not yield i
                res << i
            end
        end
        return res

	end

	def p2take(init)
		return self.p2first(init)
	end

	def p2take_while(&blk)
		arr =[]
    	p2each do |e|
      		arr << e if blk.call(e) == true
      		return arr if blk.call(e) == false
    	end
    	return arr
	end

	def p2to_a(*args)
		return self.p2entries
	end

	def p2to_h(*args) 
        arr = Hash.new
        self.p2each do |key, val|
            arr[key] = val
        end
        return arr
    end

    
end
