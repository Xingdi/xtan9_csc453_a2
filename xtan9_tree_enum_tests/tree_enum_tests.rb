require "../xtan9_tree/tree.rb"


mytree = P2tree.new(0)
mytree.left = P2tree.new(1.1)
mytree.right = P2tree.new(1.2)
mytree.left.left = P2tree.new(2.1)
mytree.left.right = P2tree.new(2.2)
mytree.right.left = P2tree.new(2.3)
mytree.right.right = P2tree.new(2.4)
mytree.right.right = P2tree.new(2.4)

mytree.left.left.left = P2tree.new(3.1)
mytree.left.left.right = P2tree.new(3.2)
mytree.left.right.left = P2tree.new(3.3)
mytree.left.right.right = P2tree.new(3.4)
mytree.right.left.left = P2tree.new(3.5)
mytree.right.left.right = P2tree.new(3.6)
mytree.right.right.left = P2tree.new(3.7)
mytree.right.right.right = P2tree.new(3.8)
@myTree = mytree


def all_test
	b = @myTree.p2all? { |word| word >= 3 } 
	c = @myTree.p2all? { |word| word < 5 } 
	raise "#{__method__} error" if !b == false and !b == true
  	p "#{__method__} passed"
end

def any_test
	b = @myTree.p2any? { |word| word >= 3 } 
  	c = @myTree.p2any? { |word| word >= 30 } #=> true
	raise "#{__method__} error" if !b == true and !c == false
  	p "#{__method__} passed"
end

def collect_test
	b = @myTree.p2collect { 2 } 
	raise "#{__method__} error" if b != [2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2]
  	p "#{__method__} passed"
end

def collect_concat_test
	b = @myTree.p2collect_concat { |e| e + 1 }
	raise "#{__method__} error" if b != [4.1, 3.1, 4.2, 2.1, 4.3, 3.2, 4.4, 1, 4.5, 3.3, 4.6, 2.2, 4.7, 3.4, 4.8]
  	p "#{__method__} passed"
end

def count_test
	b = @myTree.p2count { |e| e>=3 }
	raise "#{__method__} error" if b != 8
  	p "#{__method__} passed"
end

def cycle_test
	arr = []
	b = @myTree.p2cycle(2) { |x| arr << x }
	raise "#{__method__} error" if b != nil or arr != [3.1, 2.1, 3.2, 1.1, 3.3, 2.2, 3.4, 0, 3.5, 2.3, 3.6, 1.2, 3.7, 2.4, 3.8, 3.1, 2.1, 3.2, 1.1, 3.3, 2.2, 3.4, 0, 3.5, 2.3, 3.6, 1.2, 3.7, 2.4, 3.8]
  	p "#{__method__} passed"
end

def detect_test
	b = @myTree.p2detect(2) { |x| x == 1.1 and x + 2 == 3.1}
	raise "#{__method__} error" if b != 1.1
  	p "#{__method__} passed"
end

def drop_test
	b = @myTree.p2drop(3)
	raise "#{__method__} error" if b != [1.1, 3.3, 2.2, 3.4, 0, 3.5, 2.3, 3.6, 1.2, 3.7, 2.4, 3.8]
  	p "#{__method__} passed"
end

def drop_while_test #Xingdi did't pass########################################
	b = @myTree.drop_while{|i| i < 3}
	raise "#{__method__} error" if b != [3.1, 2.1, 3.2, 1.1, 3.3, 2.2, 3.4, 0, 3.5, 2.3, 3.6, 1.2, 3.7, 2.4, 3.8]
  	p "#{__method__} passed"
end

def each_cons_test
	ar = []
	b = @myTree.each_cons(3){|a| ar << a} 
	raise "#{__method__} error" if b != nil or ar != [[3.1, 2.1, 3.2], [2.1, 3.2, 1.1], [3.2, 1.1, 3.3], [1.1, 3.3, 2.2], [3.3, 2.2, 3.4], [2.2, 3.4, 0], [3.4, 0, 3.5], [0, 3.5, 2.3], [3.5, 2.3, 3.6], [2.3, 3.6, 1.2], [3.6, 1.2, 3.7], [1.2, 3.7, 2.4], [3.7, 2.4, 3.8]]
  	p "#{__method__} passed"
end

def each_slice_test # undefined method  'size' for p2tree
	ar = []
	b = @myTree.each_slice(3){|a| ar << a} #######################
	raise "#{__method__} error" if b != nil or ar != [[3.1, 2.1, 3.2], [1.1, 3.3, 2.2], [3.4, 0, 3.5], [2.3, 3.6, 1.2], [3.7, 2.4, 3.8]]
  	p "#{__method__} passed"
end

def each_with_index_test
	hash = Hash.new
	b = @myTree.p2each_with_index { |item, index| hash[item] = index}
	raise "#{__method__} error" if hash !={3.1=>0, 2.1=>1, 3.2=>2, 1.1=>3, 3.3=>4, 2.2=>5, 3.4=>6, 0=>7, 3.5=>8, 2.3=>9, 3.6=>10, 1.2=>11, 3.7=>12, 2.4=>13, 3.8=>14}
  	p "#{__method__} passed"
end

def entries_test
	b = @myTree.p2entries() 
	raise "#{__method__} error" if b != [3.1, 2.1, 3.2, 1.1, 3.3, 2.2, 3.4, 0, 3.5, 2.3, 3.6, 1.2, 3.7, 2.4, 3.8]
  	p "#{__method__} passed"
end

def find_test
	b = @myTree.p2find(2) { |x| x == 2.1 and x%2.1 ==0}
	raise "#{__method__} error" if b != 2.1
  	p "#{__method__} passed"
end

def find_all_test
	b = @myTree.p2find_all{|i|  i >=3 } 
	raise "#{__method__} error" if b != [3.1, 3.2, 3.3, 3.4, 3.5, 3.6, 3.7, 3.8]
  	p "#{__method__} passed"
end

def find_index_test
	b = @myTree.p2find_index{ |i| i == 2.2 }
	raise "#{__method__} error" if b != 5
  	p "#{__method__} passed"
end

def first_test
	b = @myTree.p2first(2)
	raise "#{__method__} error" if b != [3.1, 2.1]
  	p "#{__method__} passed"
end

def group_by_test
	b = @myTree.p2group_by { |i| i } 
	raise "#{__method__} error" if b !={3.1=>[3.1], 2.1=>[2.1], 3.2=>[3.2], 1.1=>[1.1], 3.3=>[3.3], 2.2=>[2.2], 3.4=>[3.4], 0=>[0], 3.5=>[3.5], 2.3=>[2.3], 3.6=>[3.6], 1.2=>[1.2], 3.7=>[3.7], 2.4=>[2.4], 3.8=>[3.8]}
  	p "#{__method__} passed"
end

def inject_test1
	b = @myTree.p2para0inject { |sum, n| sum + n }           
	raise "#{__method__} error" if b != 38.9
  	p "#{__method__} passed"
end

def inject_test2
	b = @myTree.p2inject(1) { |product, n| product * n }
	raise "#{__method__} error" if b != 0.0
  	p "#{__method__} passed"
end


def find_all_test
	b = @myTree.p2find_all{|i|  i >3 } 
	raise "#{__method__} error" if b != [3.1, 3.2, 3.3, 3.4, 3.5, 3.6, 3.7, 3.8]
  	p "#{__method__} passed"
end

def minmax_test  
	a = @myTree
	b = a.p2minmax { |a, b| a <=> b } 
	raise "#{__method__} error" if b != [0, 3.8] 
  	p "#{__method__} passed"
end

def minmax_by_test
	a = @myTree
	b = a.p2minmax_by { |a| a+1 }                           
	raise "#{__method__} error" if b != [0, 3.8] 
  	p "#{__method__} passed"
end

def partition_test
	b =@myTree.p2partition { |v| v > 2 } 
	raise "#{__method__} error" if b != [[3.1, 2.1, 3.2, 3.3, 2.2, 3.4, 3.5, 2.3, 3.6, 3.7, 2.4, 3.8], [1.1, 0, 1.2]]
  	p "#{__method__} passed"
end

def reject_test
	b = @myTree.p2reject { |i|  i >2 }  
	raise "#{__method__} error" if b != [1.1, 0, 1.2]
  	p "#{__method__} passed"
end

def take_test
	a = @myTree
	b = a.p2take(3)             
	c = a.p2take(30)          
	raise "#{__method__} error" if b != [3.1, 2.1, 3.2] or c != [3.1, 2.1, 3.2, 1.1, 3.3, 2.2, 3.4, 0, 3.5, 2.3, 3.6, 1.2, 3.7, 2.4, 3.8]
  	p "#{__method__} passed"
end

def take_while_test
	a = @myTree
	b = a.p2take_while { |i| i == 3.1 } 
	raise "#{__method__} error" if b != [3.1]
  	p "#{__method__} passed"
end

def to_a_test
	b = @myTree.p2to_a
	raise "#{__method__} error" if b != [3.1, 2.1, 3.2, 1.1, 3.3, 2.2, 3.4, 0, 3.5, 2.3, 3.6, 1.2, 3.7, 2.4, 3.8]
  	p "#{__method__} passed"
end

def to_h_test
	b = @myTree.each_with_index.p2to_h
	raise "#{__method__} error" if b != {3.1=>0, 2.1=>1, 3.2=>2, 1.1=>3, 3.3=>4, 2.2=>5, 3.4=>6, 0=>7, 3.5=>8, 2.3=>9, 3.6=>10, 1.2=>11, 3.7=>12, 2.4=>13, 3.8=>14}
  	p "#{__method__} passed"
end

class Enumerator
	include P2Enumerable
	alias p2each each
end





all_test
any_test
collect_test
collect_concat_test
count_test
cycle_test
detect_test
drop_test
drop_while_test
each_cons_test
each_slice_test
each_with_index_test
entries_test
find_test
find_all_test
find_index_test
first_test
group_by_test
inject_test1
inject_test2
find_all_test
minmax_test
minmax_by_test
partition_test
reject_test
take_test
take_while_test
to_a_test
to_h_test