require "../xtan9_enum/enum.rb"

class P2tree
  include Enumerable
  include P2Enumerable
  @s = 0
  @root
  @level = 1

  attr_accessor :data, :left, :right
  def initialize(data)
    @data = data
  end

  def p2each(&block)
    if left
      left.p2each(&block)
    end
    block.call(self.data)
    if right
      right.p2each(&block)
  end
  end

  def each(&block)
    if left
      left.each(&block)
    end
    block.call(self.data)
    if right
      right.each(&block)
    end
  end

  def p2each_with_level(level = 1,&blk)
    if left
      l = level + 1
      left.p2each_with_level(l,&blk)
    end
    blk.call(self.data,level)
    if right
      l = level + 1
      right.p2each_with_level(l,&blk)
    end
  end

end