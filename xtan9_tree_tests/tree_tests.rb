require "../xtan9_tree/tree.rb"

mytree = P2tree.new(0)
mytree.left = P2tree.new(1.1)
mytree.right = P2tree.new(1.2)
mytree.left.left = P2tree.new(2.1)
mytree.left.right = P2tree.new(2.2)
mytree.right.left = P2tree.new(2.3)
mytree.right.right = P2tree.new(2.4)
mytree.right.right = P2tree.new(2.4)

mytree.left.left.left = P2tree.new(3.1)
mytree.left.left.right = P2tree.new(3.2)
mytree.left.right.left = P2tree.new(3.3)
mytree.left.right.right = P2tree.new(3.4)
mytree.right.left.left = P2tree.new(3.5)
mytree.right.left.right = P2tree.new(3.6)
mytree.right.right.left = P2tree.new(3.7)
mytree.right.right.right = P2tree.new(3.8)
@myTree = mytree






def each_with_level_test
	arr = []
	@myTree.p2each_with_level do |x,y|
		arr << [x,y]
	end
	raise "#{__method__} error" if arr != [[3.1, 4], [2.1, 3], [3.2, 4], [1.1, 2], [3.3, 4], [2.2, 3], [3.4, 4], [0, 1], [3.5, 4], [2.3, 3], [3.6, 4], [1.2, 2], [3.7, 4], [2.4, 3], [3.8, 4]]
  	p "#{__method__} passed"
end

def each_test
	arr = []
	@myTree.p2each do |x|
		arr << x
	end
	raise "#{__method__} error" if arr != [3.1, 2.1, 3.2, 1.1, 3.3, 2.2, 3.4, 0, 3.5, 2.3, 3.6, 1.2, 3.7, 2.4, 3.8]
  	p "#{__method__} passed"
end

each_test
each_with_level_test